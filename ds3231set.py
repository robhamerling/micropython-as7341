#!/usr/bin/env python

# ds3231set.py

"""
 Rob Hamerling, Version 4.0, January 2024

 Initialize DS3231 from current RTC time
 First open an online terminal session with the ESP32
 and make an instance of rtc with:
        from machine import RTC
        rtc = RTC()
 Then set rtc to the desired UTC date and time with:
        rtc.datetime( (YYYY, MM, DD, 0, hh, mm, ss, 0) )
 and check if current time is correctly set with:
        rtc.datetime()
 Then import this script to copy the RTC date/time to the DS3231
 From then on the ESP32 date/time can be set from the DS3231
 for example from main.py with the ds3231get script.
"""

from machine import I2C, Pin
import os

import ds3231

# Select I2C bus hardware
bus = I2C(0)

ds3231 = ds3231.DS3231(bus)             # instance of DS3231 class
if ds3231.isconnected:
    print("Setting DS3231 to current RTC values", ds3231.rtc_to_ds3231())
else:
    print("DS3231 not found!")

#
