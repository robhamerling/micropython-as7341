#!/usr/bin/env python

"""
 ds3231get.py

 Rob Hamerling, Version 4.0, 2024

 Initialize RTC from DS3231

 Import this script to initialize RTC of ESP32 from DS3231 on I2C(0)
"""

from machine import I2C
import os

import ds3231

print(f'Synchronizing RTC with DS3231: ', end="")
bus = I2C(0)
ds3231 = ds3231.DS3231(bus)             # instance of DS3231 class
if ds3231.isconnected:
    Y,M,D,h,m,s = ds3231.ds3231_to_rtc()    # read date and time from DS3231
    print(f'{Y}/{M}/{D} {h:d}:{m:02d}:{s:02d} (UTC)')
else:
    print('Failed!')
#
