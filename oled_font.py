# oled_font.py

"""
This file licensed under the MIT License and incorporates work covered by
the following copyright and permission notice:

The MIT License (MIT)

Copyright (c) 2020-2022 Rob Hamerling

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
 Rob Hamerling, Version 3.3, October 2022

 Font for (text-only) OLED
 Fixed 8x8 font for ASCII values 32 .. 127.
 A character is represented by an 8x8-bits pattern.
 A character is written as 8 columns of 8 bits.
 Each byte  x..  in the matrix below represents
 a column of the character pattern.
 Since the last column is always a blank only
 the first 7 columns are specified in this table.
 The eighth column is added by the software.
"""



from micropython import const

OLED_FONT_WIDTH = const(8)
OLED_FONT_HEIGHT = const(8)
OLED_FONT_ORD_MIN = const(32)
OLED_FONT_ORD_MAX = const(127)
OLED_DISPLAY_WIDTH = const(128)
OLED_DISPLAY_HEIGHT = const(64)
OLED_DISPLAY_CHARS = OLED_DISPLAY_WIDTH // OLED_FONT_WIDTH
OLED_DISPLAY_LINES = OLED_DISPLAY_HEIGHT // OLED_FONT_HEIGHT

oled_font = (
        b'\x00\x00\x00\x00\x00\x00\x00',  # <space>
        b'\x00\x00\x00\x00\x5F\x00\x00',  # !
        b'\x00\x00\x00\x03\x00\x03\x00',  # "
        b'\x00\x24\x7E\x24\x24\x7E\x24',  # #
        b'\x00\x2E\x2A\x7F\x2A\x3A\x00',  # $
        b'\x00\x46\x26\x10\x08\x64\x62',  # %
        b'\x00\x20\x54\x4A\x54\x20\x50',  # &
        b'\x00\x00\x00\x04\x02\x00\x00',  # '
        b'\x00\x00\x00\x3C\x42\x00\x00',  # (
        b'\x00\x00\x00\x42\x3C\x00\x00',  # )
        b'\x00\x10\x54\x38\x54\x10\x00',  # *
        b'\x00\x10\x10\x7C\x10\x10\x00',  # +
        b'\x00\x00\x00\x80\x60\x00\x00',  # ,
        b'\x00\x10\x10\x10\x10\x10\x00',  # -
        b'\x00\x00\x00\x60\x60\x00\x00',  # .
        b'\x80\x40\x20\x10\x08\x04\x02',  # /
        b'\x3C\x62\x52\x4A\x46\x3C\x00',  # 0
        b'\x44\x42\x7E\x40\x40\x00\x00',  # 1
        b'\x64\x52\x52\x52\x52\x4C\x00',  # 2
        b'\x24\x42\x42\x4A\x4A\x34\x00',  # 3
        b'\x30\x28\x24\x7E\x20\x20\x00',  # 4
        b'\x2E\x4A\x4A\x4A\x4A\x32\x00',  # 5
        b'\x3C\x4A\x4A\x4A\x4A\x30\x00',  # 6
        b'\x02\x02\x62\x12\x0A\x06\x00',  # 7
        b'\x34\x4A\x4A\x4A\x4A\x34\x00',  # 8
        b'\x0C\x52\x52\x52\x52\x3C\x00',  # 9
        b'\x00\x00\x00\x48\x00\x00\x00',  # :
        b'\x00\x00\x80\x64\x00\x00\x00',  # ;
        b'\x00\x00\x10\x28\x44\x00\x00',  # <
        b'\x00\x28\x28\x28\x28\x28\x00',  # =
        b'\x00\x00\x44\x28\x10\x00\x00',  # >
        b'\x00\x04\x02\x02\x52\x0A\x04',  # ?
        b'\x3C\x42\x5A\x56\x5A\x1C\x00',  # @
        b'\x7C\x12\x12\x12\x12\x7C\x00',  # A
        b'\x7E\x4A\x4A\x4A\x4A\x34\x00',  # B
        b'\x3C\x42\x42\x42\x42\x24\x00',  # C
        b'\x7E\x42\x42\x42\x24\x18\x00',  # D
        b'\x7E\x4A\x4A\x4A\x4A\x42\x00',  # E
        b'\x7E\x0A\x0A\x0A\x0A\x02\x00',  # F
        b'\x3C\x42\x42\x52\x52\x34\x00',  # G
        b'\x7E\x08\x08\x08\x08\x7E\x00',  # H
        b'\x00\x42\x42\x7E\x42\x42\x00',  # I
        b'\x30\x40\x40\x40\x40\x3E\x00',  # J
        b'\x7E\x08\x08\x14\x22\x40\x00',  # K
        b'\x7E\x40\x40\x40\x40\x40\x00',  # L
        b'\x7E\x04\x08\x08\x04\x7E\x00',  # M
        b'\x7E\x04\x08\x10\x20\x7E\x00',  # N
        b'\x3C\x42\x42\x42\x42\x3C\x00',  # O
        b'\x7E\x12\x12\x12\x12\x0C\x00',  # P
        b'\x3C\x42\x42\x52\x62\x7C\x80',  # Q
        b'\x7E\x12\x12\x12\x32\x4C\x00',  # R
        b'\x24\x4A\x4A\x4A\x4A\x30\x00',  # S
        b'\x02\x02\x02\x7E\x02\x02\x02',  # T
        b'\x3E\x40\x40\x40\x40\x3E\x00',  # U
        b'\x1E\x20\x40\x40\x20\x1E\x00',  # V
        b'\x3E\x40\x20\x20\x40\x3E\x00',  # W
        b'\x42\x24\x18\x18\x24\x42\x00',  # X
        b'\x02\x04\x08\x70\x08\x04\x02',  # Y
        b'\x42\x62\x52\x4A\x46\x42\x00',  # Z
        b'\x00\x00\x7E\x42\x42\x00\x00',  # [
        b'\x02\x04\x08\x10\x20\x40\x80',  # <backslash>
        b'\x00\x00\x42\x42\x7E\x00\x00',  # ]
        b'\x00\x08\x04\x02\x04\x08\x00',  # ^
        b'\x80\x80\x80\x80\x80\x80\x80',  # _
        b'\x00\x00\x00\x02\x04\x00\x00',  # `
        b'\x00\x20\x54\x54\x54\x78\x00',  # a
        b'\x00\x7E\x48\x48\x48\x30\x00',  # b
        b'\x00\x00\x38\x44\x44\x44\x00',  # c
        b'\x00\x30\x48\x48\x48\x7E\x00',  # d
        b'\x00\x38\x54\x54\x54\x48\x00',  # e
        b'\x00\x00\x00\x7C\x0A\x02\x00',  # f
        b'\x00\x18\xA4\xA4\xA4\x7C\x00',  # g
        b'\x00\x7E\x08\x08\x08\x70\x00',  # h
        b'\x00\x00\x00\x48\x7A\x40\x00',  # i
        b'\x00\x00\x40\x80\x80\x7A\x00',  # j
        b'\x00\x7E\x18\x24\x40\x00\x00',  # k
        b'\x00\x00\x02\x7E\x40\x40\x00',  # l
        b'\x00\x7C\x04\x78\x04\x78\x00',  # m
        b'\x00\x7C\x04\x04\x04\x78\x00',  # n
        b'\x00\x38\x44\x44\x44\x38\x00',  # o
        b'\x00\xFC\x24\x24\x24\x18\x00',  # p
        b'\x00\x18\x24\x24\x24\xFC\x80',  # q
        b'\x00\x00\x78\x04\x04\x04\x00',  # r
        b'\x00\x48\x54\x54\x54\x20\x00',  # s
        b'\x00\x00\x04\x3E\x44\x40\x00',  # t
        b'\x00\x3C\x40\x40\x40\x3C\x00',  # u
        b'\x00\x0C\x30\x40\x30\x0C\x00',  # v
        b'\x00\x3C\x40\x38\x40\x3C\x00',  # w
        b'\x00\x44\x28\x10\x28\x44\x00',  # x
        b'\x00\x1C\xA0\xA0\xA0\x7C\x00',  # y
        b'\x00\x44\x64\x54\x4C\x44\x00',  # z
        b'\x00\x08\x08\x76\x42\x42\x00',  # {
        b'\x00\x00\x00\x7E\x00\x00\x00',  # |
        b'\x00\x42\x42\x76\x08\x08\x00',  # }
        b'\x00\x00\x04\x02\x04\x02\x00',  # ~
        b'\x00\x00\x00\x00\x00\x00\x00')  # <DEL>

#
