#!/usr/bin/env python

# This script - running on the local PC - performs 2 functions:
# 1. Initialize remote RTC with UTC from the current PC date/time
# 2. Initialize the DS3231 module by running the 'ds3231set' script remotely
# Requirement: installation of 'mpremote' on the local PC.

# This is an automation of the procedure described in the 'ds3231set.py' script.

# Note: Thonny may override time with PC time!
#       This can be prevented: in Options -> Tools -> Interpreter
#       uncheck "Synchronize device's real time clock"

import os, sys, time

def run(cmd):
    output = os.popen(cmd)          # issue command
    loglist = output.readlines()    # stdout lines
    return output.close()

t = time.gmtime()                   # UTC!
tstr = f"({t.tm_year},{t.tm_mon},{t.tm_mday},0,{t.tm_hour},{t.tm_min},{t.tm_sec},0)"
cmd1 = "'from machine import RTC; rtc=RTC(); rtc.datetime(" + tstr + ")'"
cmd2 = "'import ds3231set'"

print("Initializing RTC and DS3231 to:", tstr)
try:
    run("mpremote exec " + cmd1)
    run("mpremote exec " + cmd2)
except:
    print("mpremote command failed")
    exit(1)
